var calendar;

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ],
        eventSources: [
            {
                events: getTasks //pass a reference to a function, so we have a dynamic, updateable event source
            }
        ],
        eventClick: function (info) {
            // console.log(info.event.title);
            calendar.rerenderEvents();

        },
    });

    calendar.render();
});

var getTasks = function(fetchInfo, successCallback, failureCallback) { //the input parameters are the ones shown in the fullCalendar documentation

    var selected_project = $("input[name*='projects']:checked").val();
    var events = [];

    $.ajax({
        url: route('calendar.tasks'),
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            project: selected_project,
        },
        success: function (response) {

            // console.log(response);
            response.forEach(task => {
                var obj = {};
                obj.title = task.body + ' - ' + task.assigned_to.name;
                obj.start = task.deadline;
                obj.allDay = true;
                obj.classNames = 'bg-' + task.status.color + ' text-dark';
                obj.url = task.path;

                events.push(obj);
            });
            successCallback(events); //pass the event data to fullCalendar via the provided callback function
        },
        error: function(response) {
            var err = jQuery.parseJSON(response.responseText);
            console.log(err.message);
            failureCallback(response); //inform fullCalendar of the error via the provided callback function
        }
    });
}

$(document).ready(function () {
    $('input[type=radio][name=projects]').change(function() {
        calendar.refetchEvents();
    });
});
