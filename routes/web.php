<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    // Projects
    Route::get('/', 'ProjectsController@index');
    Route::get('/projects', 'ProjectsController@index')->name('projects');
    Route::get('/projects/create', 'ProjectsController@create');
    Route::get('/projects/{project}', 'ProjectsController@show')->middleware('can:view,project')->name('project.show');
    Route::get('/projects/{project}/edit', 'ProjectsController@edit')->middleware('can:update,project');
    Route::patch('/projects/{project}', 'ProjectsController@update')->middleware('can:update,project');
    Route::post('/projects', 'ProjectsController@store');
    Route::delete('/projects/{project}', 'ProjectsController@destroy')->middleware('can:delete,project');

    // Tasks
    Route::get('/projects/{project}/tasks/create', 'ProjectTasksController@create')->middleware('can:create,App\Models\Task,project');
    Route::get('/projects/{project}/tasks/{task}', 'ProjectTasksController@show')->middleware('can:view,task');
    Route::get('/projects/{project}/tasks/{task}/edit', 'ProjectTasksController@edit')->middleware('can:update,task');
    Route::post('/projects/{project}/tasks', 'ProjectTasksController@store')->middleware('can:create,App\Models\Task,project');
    Route::post('/projects/{project}/tasks/{task}/statusChanges', 'ProjectTasksController@statusChanges')->middleware('can:update,task');
    Route::patch('/projects/{project}/tasks/{task}', 'ProjectTasksController@update')->middleware('can:update,task');
    Route::delete('/projects/{project}/tasks/{task}', 'ProjectTasksController@destroy')->middleware('can:delete,task');

    Route::get('/home', 'HomeController@index')->name('home');

    // Notifications
    Route::get('/notifications/markAsRead/{notification}', 'Notifications@markAsRead')->name('markRead');
    Route::get('/notifications/markAllAsRead', 'Notifications@markAllAsRead')->name('markAllRead');

    // Messages
    Route::get('/conversations', 'ConversationController@index')->name('message.index');
    Route::get('/conversations/create', 'ConversationController@create')->name('message.create');
    Route::get('/conversations/markAllAsRead', 'ConversationController@markAllAsRead')->name('message.markAllAsRead');
    Route::get('/conversations/{user}', 'ConversationController@show')->name('message.show');
    Route::get('/conversations/markAsRead/{message}', 'ConversationController@markAsRead')->name('message.markAsRead');
    Route::post('/conversations/sendMessage', 'ConversationController@sendMessage')->name('message.send');

    // Calendar
    Route::get('/calendar', 'CalendarController@index')->name('calendar.index');
    Route::post('/calendar/getTasks', 'CalendarController@getTasks')->name('calendar.tasks');

    // Charts
    Route::get('/charts', 'ChartsController@index')->name('charts.index');
    Route::post('/charts/getTasks', 'ChartsController@getTasks')->name('charts.tasks');

    // Localization
    Route::get('welcome/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    })->name('locale');

});

Auth::routes();
