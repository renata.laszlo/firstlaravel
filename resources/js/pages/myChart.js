var chart;

$(document).ready(function() {

    /**  initial daterangepicker values */
    var start = moment().startOf('month');
    var end = moment().endOf('month');

    $('#daterange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);

    /** options for the chart */
    var options = {
        chart: {
            renderTo: 'chart',
            events: {
                load: getTasks,
            }
        },
        title: {
            text: 'Tasks'
        },
        xAxis:
        [ {
            type: 'datetime',
            startOnTick: true,
            // minPadding: 1,
            min: start,
            max: end,
            dateTimeLabelFormats:
            {
                day: "%e",
            },
        }, {
            labels: {
                format: '{value:%b}'
            },
        } ],
        series: [],
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        lang: {
            noData: "No data"
        },
    }

    /** create a chart */
    chart = new Highcharts.ganttChart('chart', options);

    /** select a date */
    $('#daterange').on('apply.daterangepicker', function(ev, picker) {
        $('#startdate').val(picker.startDate);
        $('#enddate').val(picker.endDate);

        options.xAxis[0].min = picker.startDate;
        options.xAxis[0].max = picker.endDate;
        chart = new Highcharts.ganttChart('chart', options);
    });

});

/** change the dateranger label, set the startDate and endDate */
function cb(start, end) {
    $('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#startdate').val(start);
    $('#enddate').val(end);
}

/** set the series of chart */
function getTasks() {
    $.ajax({
        url: route('charts.tasks'),
        type: "POST",
        data : {
            start : $('#startdate').val(),
            end: $('#enddate').val(),
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {

            if (response.length == 0){
                $('#chart').html( "<div class='text-center mt-3'><h3>No data available for selected date range</h3></div>" );
            }

            response.forEach(project => {

                // project
                var myData = [];
                myData.push({
                    name: project.title,
                    id: 'project_' + project.id,
                    start: new Date(project.created_at).getTime(),
                    end: new Date(project.deadline).getTime()
                });
                // tasks
                project.tasks.forEach(task => {
                    var obj = {
                        name: task.body,
                        parent: 'project_' + project.id,
                        id: 'task_' + task.id,
                        start: new Date(task.created_at).getTime(),
                        end: new Date(task.deadline).getTime(),
                        completed: task.completed,
                    }
                    myData.push(obj);

                });
                // series
                chart.addSeries({
                    name: project.title,
                    data: myData
                });

            });

        },
        cache: false
    });

}
