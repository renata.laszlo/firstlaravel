// window.onload = function () {

    $(document).ready(function(e){

        $('#messages').scrollTop($('#messages').prop('scrollHeight'));

        $('#sendMessage').on('click', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: route('message.send'),
                method: 'post',
                data: {
                    receiver_id: $('#receiver_id').val(),
                    body: $('#body').val()
                },
                success: function(result){
                    $('div.alert').remove();
                    $('#messages').html(result);
                    $('#body').val('');
                    $('#messages').scrollTop($('#messages').prop('scrollHeight'));
                },
                error: function(err) {
                    $.each(err.responseJSON.errors, function(key,value) {
                        $('div.alert').remove();
                        $('#'+key).after('<div class="alert alert-danger">'+value+'</div');
                    });
                }
            });

        });
    });

    // });
// }
