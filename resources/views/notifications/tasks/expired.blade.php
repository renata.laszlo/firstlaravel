@component('mail::message')
# This task expired.

@include('tasks.show', ['task' => $task])

@component('mail::button', ['url' => url($task->path())])
View the task: {{ $task->body }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
