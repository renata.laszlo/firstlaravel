@component('mail::message')
# Assigned to a Task

{{ $task->project->owner->name }} assigned you to a task.
{{-- @include('tasks.includes.item', $task) --}}

@component('mail::button', ['url' => url($task->path())])
View the task: {{ $task->body }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
