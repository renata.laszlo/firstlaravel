@component('mail::message')
# This task expires soon.

@include('tasks.show', ['task' => $task])

@component('mail::button', ['url' => url($task->path())])
View the task: {{ $task->body }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
