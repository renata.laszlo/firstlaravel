@extends('layouts.app')

@section('content')


        <header class="py-4 d-flex justify-content-between align-items-end">
            <h2 class="">{{ __('task.my_projects') }}</h2>

            <a class="btn bg-info text-white" href="/projects/create">New project</a>
        </header>

        <div class="d-flex flex-wrap">
            @forelse ($projects as $project)
                <div class="pr-4 pb-4 w-50">
                    @include('projects.includes.card')
                </div>
            @empty
                <div>No projects yet.</div>
            @endforelse
        </div>


@endsection
