@extends('layouts.app')

@section('content')

    <div class="w-50 mx-auto bg-white p-4 p-5 rounded shadow">

        <h1 class="mb-10 text-center">
            Edit this project
        </h1>

        <form action="{{ $project->path() }}" method="POST" >
            @method('PATCH')

            @include('projects.includes.form', ['buttonText' => 'Update project'])

        </form>
    </div>

@endsection
