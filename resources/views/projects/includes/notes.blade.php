<form action="{{ $project->path() }}" method="POST">
    @method('PATCH')
    @csrf

    <textarea
        name="notes"
        class="card w-100 mp-4"
        style="min-height: 200px"
        placeholder="notes..."
        >{{ $project->notes }}</textarea>
    <button type="submit" class="btn bg-info text-white mt-3">Save</button>
</form>
