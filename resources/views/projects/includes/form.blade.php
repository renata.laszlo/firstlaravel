@csrf

<div class="mb-6">
    <label for="title" class="text-sm mb-2 block">Title</label>

    <div class="form-group">
        <input
            type="text"
            placeholder="Project title"
            name="title"
            class="form-control rounded w-100 p-2 @error('title') is-invalid @enderror"
            value="{{ old('title') ?? $project->title }}">

        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="pb-2">
    <label for="description" class="my-2">Description</label>

    <div class="form-group">
        <textarea
            class="form-control w-100 p-2 @error('description') is-invalid @enderror"
            rows="10"
            name="description"
            id="description"
            placeholder="Describe your project.">{{ old('body') ?? $project->description }}</textarea>

        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="justify-content-between d-flex align-items-center">
    <button class="btn bg-info text-white" type="submit">{{ $buttonText }}</button>

    <a href="{{ $project->path() }}">Cancel</a>
</div>
