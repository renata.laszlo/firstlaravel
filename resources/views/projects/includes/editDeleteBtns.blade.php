<div class="pr-2">
    <a class="btn bg-info text-white" href="{{ $project->path() . '/edit' }}">Edit project</a>
</div>

<form action="{{ $project->path() }}" method="POST">
    @method('DELETE')
    @csrf
    <button onclick="return confirm('Are you sure?');" class="btn bg-danger text-white " type="submit">Delete project</button>
</form>
