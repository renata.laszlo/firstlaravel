<div class="card h-100">
    <h3 class="py-3 mb-3 ml-n4 pl-4 border-left border-info border-4">
        <a class="text-body" href="{{ $project->path() }}">{{ $project->title }}</a>
    </h3>

    <div class="text-muted">{{ str_limit($project->description, 150) }}</div>

</div>
