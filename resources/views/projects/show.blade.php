@extends('layouts.app')

@section('content')

    <header class="py-4 d-flex align-items-end">
        <p class="mr-auto">
            <a class="text-muted" href="/projects">
                My projects
            </a> / {{ $project->title }}
        </p>

        @includeWhen($isOwner, 'projects.includes.editDeleteBtns', $project)

    </header>

    <main>
        <div class="d-flex mx-n3">

            <div class="w-75 px-3">

                <div class="mb-8">
                    <h4 class="text-muted mb-3">{{ __('task.my_tasks') }}</h4>

                    @include('tasks.index')

                    @can('update', $project)
                        <div class="p-0 card mb-3">
                            <a href="{{ $project->path() }}/tasks/create">Add a new task...</a>
                        </div>
                    @endcan

                </div>

                <div>
                    <h4 class="text-muted mb-3">{{ __('task.general_notes') }}</h4>

                    @include('projects.includes.notes')
                </div>

            </div>

            <div class="w-25 px-3">
                <div class="h-auto">
                    @include('projects.includes.card')
                </div>
            </div>

        </div>
    </main>



@endsection
