@extends('layouts.app')

@section('content')


    <div class="p-5 w-50 mx-auto bg-white rounded shadow">

        <h2 class="text-center">
            Create new project
        </h2>

        <form action="/projects" method="POST" >

            @include('projects.includes.form',
                ['project' => new App\Models\Project,
                'buttonText' => 'Save project'])

        </form>
    </div>

@endsection
