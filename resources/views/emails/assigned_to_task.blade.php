@component('mail::message')
# New Task

@include('tasks.includes.item', $task)

@component('mail::button', ['url' => '{{ $task->path() }}'])
View the task {{ url($task->path()) }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
