<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fa fa-bell"></i>

        @if ( $num_of_not = auth()->user()->unreadNotifications->count())
            <span class="badge badge-danger">{{ $num_of_not }} </span>
        @endif

    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

        @if ($num_of_not)
            <a class=" dropdown-item text-success" href="{{ route('markAllRead') }}">Mark all as Read</a>

            @foreach (auth()->user()->unreadNotifications as $notification)
                <a class="dropdown-item" href=" {{ route('markRead', ['notification' => $notification->id]) }}">
                    {{$notification->data['msg'] .': ' . $notification->data['task']['body'] }}
                </a>
            @endforeach
        @else
            <a class="dropdown-item" href="#">Notify</a>
            {{-- <a class="dropdown-item" href="/notifications">Notify</a> --}}
        @endif

    </div>
</li>
