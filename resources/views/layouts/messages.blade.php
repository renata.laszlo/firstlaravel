<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fa fa-envelope"></i>

        @if ( $num_of_mess = auth()->user()->unreadMessages->count())
            <span class="badge badge-danger">{{ $num_of_mess }} </span>
        @endif

    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

        @if ($num_of_mess)
            <a class=" dropdown-item text-success" href="{{ route('message.markAllAsRead') }}">Mark all as Read</a>

            @foreach (auth()->user()->unreadMessages as $message)
                <a class="dropdown-item" href=" {{ route('message.markAsRead', ['message' => $message]) }}">
                    {{$message->sender->name}} - {{ substr($message->body, 0, 5) }}...
                </a>
            @endforeach
        @else
            <a class="dropdown-item" href="{{ route('message.index') }}">Messages</a>
        @endif

    </div>
</li>
