@forelse ($messages as $message)

    <div class="mx-2">

        @if (auth()->id() == $message->sender->id)
            <div class="text-right">
                <p class="mb-2 mt-3">{{ $message->sender->name }}
                    <span class="text-muted font-italic">- {{ date('M d  H:i', strtotime( $message->created_at)) }}</span>
                </p>
                <label class="p-2 rounded m-0 text-white bg-info">{{ $message->body }}</label>
            </div>
        @else
            <div class="text-left">
                <p class="mb-2 mt-3">{{ $message->sender->name }}
                    <span class="text-muted font-italic">- {{ date('M d  H:i', strtotime( $message->created_at)) }}</span>

                    @if ( ! $message->read_at)
                        <span class="badge badge-pill badge-danger">New</span>
                    @endif
                </p>
                <label class="p-2 rounded m-0 text-white bg-success">{{ $message->body }}</label>
            </div>
        @endif

    </div>
@empty

    <div class="">
        <h3 class="text-muted font-italic text-center">No message yet.</h3>
    </div>

@endforelse
