@extends('layouts.app')

@section('content')


    <div class="p-5 w-50 mx-auto bg-white rounded shadow">

        <h2 class="text-center">
            Send a message
        </h2>

        <form action="{{ route('message.send') }}" method="POST">
            @csrf

            <div class="pb-2">
                <label for="receiver_id" class="my-2">Send to</label>

                <div class="form-group">
                    <select name="receiver_id" id="receiver_id" class="form-control custom-select mb-3 @error('receiver_id') is-invalid @enderror">
                        @foreach ($users as $id => $name)
                            <option

                                value="{{ $id }}">
                                {{ $name }}
                            </option>
                        @endforeach
                    </select>

                    @error('receiver_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="pb-2">
                <label for="body" class="my-2">Description</label>

                <div class="form-group">
                    <textarea
                        class="w-100 p-2 form-control @error('body') is-invalid @enderror"
                        rows="10"
                        name="body"
                        id="body"
                        placeholder="Write your message.">{{ old('body') ?? '' }}</textarea>

                    @error('body')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>


            <div class="justify-content-between d-flex align-items-center">
                <button class="btn bg-info text-white" type="submit">Send</button>

                <a href="/projects">Cancel</a>
            </div>

        </form>
    </div>

@endsection
