@extends('layouts.app')

@section('content')

    <div id="chat">
        <div class="d-flex flex-wrap border shadow h-100">
            <div class="w-25 bg-secondary text-white text-center ">
                <div class="d-flex flex-column list-group" id="users">

                    <input type="hidden" id="receiver_id" name="receiver_id" value="{{ $selected_user->id }}">

                    @foreach ($users as $user)

                        <a class="text-white list-group-item bg-secondary list-group-item-action border-0 {{ $user->id == $selected_user->id ? 'active' : '' }}"
                            href="{{ route('message.show', ['user' => $user->id]) }}">{{ $user->name }}</a>

                    @endforeach
                </div>
            </div>

            <div class="w-75 bg-white p-2 h-100">
                <div class="d-flex flex-column h-100 justify-content-between">

                    <div id="messages">
                        @include('messages.includes.items', ['messages' => $messages])
                    </div>

                    <div class="">
                        <div class="mt-3">
                            <textarea
                                placeholder="Write a message..."
                                class="w-100 bg-light rounded p-3 @error('body') is-invalid @enderror"
                                name="body"
                                id="body"
                                rows="6"></textarea>

                            @error('body')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="text-right">
                            <button id="sendMessage" type="button" class="btn btn-primary">Send</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

@push('scripts')
    @routes
    <script src="{{ asset('js/myMessage.js') }}"></script>
@endpush
