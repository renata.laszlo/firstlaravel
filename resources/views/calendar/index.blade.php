@extends('layouts.app')

@push('styles')
    <link href='{{ asset('fullcalendar/core/main.min.css') }}' rel='stylesheet' />
    <link href='{{ asset('fullcalendar/daygrid/main.min.css') }}' rel='stylesheet' />
@endpush

@section('content')

    <div class="container">
        <div class="card card-default">
            <div class="card-header border-0">

                <h4>Projects</h4>
                <div class="custom-control custom-radio">
                    <div>
                        <input type="radio" class="custom-control-input" value="0" id="project_all" name="projects" {{ $selected_project == 0 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="project_all">All projects</label>
                    </div>
                    @foreach ($projects as $item)
                        <div>
                            <input type="radio" class="custom-control-input" value="{{ $item->id }}" id="project_{{ $item->id }}" name="projects" {{ $selected_project == $item->id ? 'checked' : '' }} >
                            <label class="custom-control-label" for="project_{{ $item->id }}">{{ $item->title }}</label>
                        </div>
                    @endforeach
                </div>

            </div>

            <div class="card-body">
                <div>
                    @foreach ($statuses as $status)
                        <span class="badge badge-{{ $status->color }}">{{ $status->title }}</span>
                    @endforeach
                </div>

                <div id='calendar'></div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('fullcalendar/core/main.min.js') }}" defer></script>
    <script src='{{ asset('fullcalendar/daygrid/main.min.js') }}' defer></script>
    {{-- <script src="{{ asset('js/vendor.js') }}" defer></script> --}}

    @routes
    <script src='{{ asset('js/myCalendar.js') }}' ></script>

@endpush
