<div class="d-flex flex-wrap">
    @foreach ($tasks as $tasks_list)
        <div class="w-50">
            <div class="shadow border m-2 bg-white">

                <div class="bg-{{ $tasks_list->first()->status->color }} p-2 rounded d-flex justify-content-between align-items-center text-white">
                    <h5 class="m-0">{{ $tasks_list->first()->status->title }}</h5>

                    <p class="m-0">{{ count($tasks_list) }}</p>
                </div>

                <div class="tasks pre-scrollable">

                    @each('tasks.includes.item', $tasks_list, 'task')

                </div>

            </div>
        </div>
    @endforeach
</div>
