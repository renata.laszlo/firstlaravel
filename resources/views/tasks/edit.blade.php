@extends('layouts.app')

@section('content')


    <div class="p-5 w-50 mx-auto bg-white rounded shadow">

        <h2 class="text-center">
            Edit the task
        </h2>

        <form action="{{ $task->path() }}" method="POST">
            @csrf
            @method('PATCH')

            @if ($isOwner)
                @include('tasks.includes.form',
                ['project' => $project,
                'task' => $task,
                'buttonText' => 'Save'])
            @else
                @include('tasks.includes.assignedForm',
                ['project' => $project,
                'task' => $task,
                'buttonText' => 'Save'])
            @endif



        </form>
    </div>

@endsection
