@extends('layouts.app')

@section('content')


    <div class="p-5 w-50 mx-auto bg-white rounded shadow">

        <h2 class="text-center">
            Add a new task to "{{ $project->title }}"
        </h2>

        <form action="{{ $project->path() . '/tasks' }}" method="POST">
            @csrf

            @include('tasks.includes.form',
            ['project' => $project,
            'task' => new App\Models\Task,
            'buttonText' => 'Save'])

        </form>
    </div>

@endsection
