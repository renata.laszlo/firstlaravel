<div class="p-2 border-bottom">
    <form method="POST" action="{{ $task->path(). '/statusChanges' }}">
        @csrf
        {{-- @method('PATCH') --}}

        <div class="d-flex">

            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="completed{{$task->id}}" name="completed"
                    onchange="this.form.submit()" {{ $task->completed_at ? 'checked' : '' }}>
                <label class="custom-control-label" for="completed{{$task->id}}"></label>
            </div>

            <a class="w-100 border-0 text-dark" href="{{ $task->path(). '/edit' }}">{{ $task->body }}</a>
            {{-- <input class="w-100 border-0 {{ $task->completed ? 'text-success' : '' }}" name="body" value="{{ $task->body }}"> --}}

            <div>
                <span class="badge badge-{{ $task->priority->color }}">{{ $task->priority->title }}</span>
            </div>


        </div>
        <div class="d-flex text-muted justify-content-between">

            <div class="">
                <i class="fa fa-user-o"></i>
                {{ $task->assignedTo->name }}
            </div>

            <div class="font-italic">
                <i class="fa fa-clock-o"></i>
                {{ date("M d", strtotime($task->deadline)) }}
            </div>

        </div>

    </form>
</div>
