
@csrf

<div class="pb-2">
    <label for="body" class="my-2">Description</label>

    <div class="form-group">
        {{ $task->body }}
    </div>
</div>

<div class="pb-2">
    <label for="notes" class="my-2">Notes</label>

    <div class="form-group">
        <textarea
            class="w-100 p-2 form-control @error('notes') is-invalid @enderror"
            rows="10"
            name="notes"
            id="notes"
            placeholder="Write a comment....">{{ old('notes') ?? $task->notes }}</textarea>

        @error('notes')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="pb-2">
    <label for="estimated_hours" class="my-2">Estimated Hours</label>

    <div class="form-group">
        <input
            class="w-100 form-control @error('estimated_hours') is-invalid @enderror"
            type="number"
            name="estimated_hours"
            id="estimated_hours"
            value="{{ old('estimated_hours') ?? $task->estimated_hours }}">

        @error('estimated_hours')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="pb-2">
    <label for="total_hours" class="my-2">Total Hours</label>

    <div class="form-group">
        <input
            class="w-100 form-control @error('total_hours') is-invalid @enderror"
            type="number"
            name="total_hours"
            id="total_hours"
            value="{{ old('total_hours') ?? $task->total_hours }}">

        @error('total_hours')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="pb-2">
    <label for="status_id" class="my-2">Status</label>

    <div class="form-group">
        <select name="status_id" class="form-control custom-select mb-3 @error('status_id') is-invalid @enderror">
            @foreach ($statuses as $status)
                <option
                    {{ $status->id == $task->status_id ? 'selected' : '' }}
                    value="{{ $status->id }}">
                    {{ $status->title }}
                </option>
            @endforeach
        </select>

        @error('status_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="justify-content-between d-flex align-items-center">
    <button class="btn bg-info text-white" type="submit">{{ $buttonText }}</button>

    <a href="{{ $project->path() }}">Cancel</a>
</div>
