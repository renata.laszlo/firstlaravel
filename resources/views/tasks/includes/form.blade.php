
@csrf

<div class="pb-2">
    <label for="body" class="my-2">Description</label>

    <div class="form-group">
        <textarea
            class="w-100 p-2 form-control @error('body') is-invalid @enderror"
            rows="10"
            name="body"
            id="body"
            placeholder="Describe your task.">{{ old('body') ?? $task->body }}</textarea>

        @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="pb-2">
    <label for="assigned_to" class="my-2">Assigned to</label>

    <div class="form-group">
        <select name="assigned_to" id="assigned_to" class="form-control custom-select mb-3 @error('assigned_to') is-invalid @enderror">
            @foreach ($users as $user)
                <option
                    {{ $user->id == $task->assigned_to ? 'selected' : '' }}
                    value="{{ $user->id }}">
                    {{ $user->name }}
                </option>
            @endforeach
        </select>

        @error('assigned_to')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="pb-2">
    <label for="priority_id" class="my-2">Priority</label>

    <div class="form-group">
        <select name="priority_id" id="priority_id" class="form-control custom-select mb-3 @error('priority_id') is-invalid @enderror">
            @foreach ($priorities as $priority)
                <option
                    {{ $priority->id == $task->priority_id ? 'selected' : '' }}
                    value="{{ $priority->id }}">
                    {{ $priority->title }}
                </option>
            @endforeach
        </select>

        @error('priority_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>


<div class="pb-2">
    <label for="status_id" class="my-2">Status</label>

    <div class="form-group">
        <select name="status_id" class="form-control custom-select mb-3 @error('status_id') is-invalid @enderror">
            @foreach ($statuses as $status)
                <option
                    {{ $status->id == $task->status_id ? 'selected' : '' }}
                    value="{{ $status->id }}">
                    {{ $status->title }}
                </option>
            @endforeach
        </select>

        @error('status_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>


<div class="pb-2">
        <label for="estimated_hours" class="my-2">Estimated Hours</label>

        <div class="form-group">
            <input
                class="w-100 form-control @error('estimated_hours') is-invalid @enderror"
                type="number"
                name="estimated_hours"
                id="estimated_hours"
                value="{{ old('estimated_hours') ?? 0 }}">

            @error('estimated_hours')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="pb-2">
        <label for="total_hours" class="my-2">Total Hours</label>

        <div class="form-group">
            <input
                class="w-100 form-control @error('total_hours') is-invalid @enderror"
                type="number"
                name="total_hours"
                id="total_hours"
                value="{{ old('total_hours') ?? 0 }}">

            @error('total_hours')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>

<div class="pb-2 deadline">
    <label for="deadline" class="my-2">Deadline</label>

    <div class="form-group">

        <datetimepicker date="'{{ $task->deadline }}'" id="deadline"></datetimepicker>

        @error('deadline')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>




<div class="justify-content-between d-flex align-items-center">
    <button class="btn bg-info text-white" type="submit">{{ $buttonText }}</button>

    <a href="{{ $project->path() }}">Cancel</a>
</div>
