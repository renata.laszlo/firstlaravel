@extends('layouts.app')

@section('content')

    <header class="py-4 d-flex align-items-end">
        <p class="mr-auto">
            <a class="text-muted" href="/projects">
                My projects
            </a>
            /
            <a class="text-muted" href="{{ $task->project->path() }}">
                {{ $task->project->title }}
            </a>
            /
            {{ $task->body }}
        </p>

    </header>

    <main>
        <div class="p-2 card">

            <div class="d-flex">
                <a class="w-100 border-0" href="{{ $task->path(). '/edit' }}">{{ $task->body }}</a>

                <div>
                    <span class="badge badge-{{ $task->priority->color }}">{{ $task->priority->title }}</span>
                </div>

            </div>
            <div class="d-flex text-muted justify-content-between">

                <div class="">
                    <i class="fa fa-user-o"></i>
                    {{ $task->assignedTo->name }}
                </div>

                <div class="font-italic">
                    <i class="fa fa-clock-o"></i>
                    {{ date("M d", strtotime($task->deadline)) }}
                </div>

            </div>

            <div class="d-flex text-muted justify-content-between">
                <span class="badge badge-{{ $task->status->color }}">{{ $task->status->title }}</span>

                <div class="font-italic">
                    <i class="fa fa-hourglass-end"></i>
                    {{ $task->total_hours . ' / ' . $task->estimated_hours . ' h'}}
                </div>
            </div>

        </div>

    </main>



@endsection
