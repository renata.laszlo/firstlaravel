@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}" />
@endpush

@section('content')

    <div id="daterange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
        <i class="fa fa-calendar"></i>&nbsp;
        <span></span> <i class="fa fa-caret-down"></i>
        <input type="hidden" id="startdate">
        <input type="hidden" id="enddate">
    </div>

    <div id="chart"></div>

@endsection

@push('scripts')
    {{-- <script src="{{ asset('js/vendor.js') }}"></script> --}}

    <script src="{{ asset('gantt/highcharts-gantt.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}" defer></script>
    @routes
    <script src="{{ asset('js/myChart.js') }}"></script>
@endpush
