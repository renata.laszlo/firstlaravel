<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Task_Status;
use Faker\Generator as Faker;

$factory->define(Task_Status::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'name' => $faker->word,
        'color' => $faker->word
    ];
});
