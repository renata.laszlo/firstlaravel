<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Task;
use Faker\Generator as Faker;
use App\Models\Project;
use App\Models\User;
use App\Models\Task_Priority;
use App\Models\Task_Status;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'body' => $faker->sentence,
        'project_id' => factory(Project::class),
        'assigned_to'=> function () {
            return factory(User::class)->create()->id;
        },
        'priority_id'=> function () {
            return factory(Task_Priority::class)->create()->id;
        },
        'status_id'=> function () {
            return factory(Task_Status::class)->create()->id;
        },
    ];
});
