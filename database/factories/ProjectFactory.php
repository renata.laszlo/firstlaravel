<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Models\Project;
use App\Models\User;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4),
        'description' => $faker->sentence(5),
        'owner_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});
