<?php

use Illuminate\Database\Seeder;
use App\Models\Task_Status;

class TaskStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_status')->delete();

        $statuses = [ [
            'title' => 'Not Started',
            'name' => 'not_started',
            'color' => 'warning',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'In Progress',
            'name' => 'in_progress',
            'color' => 'info',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'Done',
            'name' => 'done',
            'color' => 'success',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'Canceled',
            'name' => 'canceled',
            'color' => 'secondary',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'Expired',
            'name' => 'expired',
            'color' => 'danger',
            'created_at' => now(),
            'updated_at' => now(),
        ] ];

        DB::table('task_status')->insert($statuses);
    }
}
