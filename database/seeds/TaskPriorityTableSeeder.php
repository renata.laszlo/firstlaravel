<?php

use Illuminate\Database\Seeder;
use App\Models\Task_Priority;

class TaskPriorityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_priority')->delete();

        $priorities = [ [
            'title' => 'Normal',
            'name' => 'normal',
            'color' => 'info',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'High',
            'name' => 'high',
            'color' => 'danger',
            'created_at' => now(),
            'updated_at' => now(),
        ], [
            'title' => 'Low',
            'name' => 'low',
            'color' => 'success',
            'created_at' => now(),
            'updated_at' => now(),
        ] ];

        DB::table('task_priority')->insert($priorities);
    }
}
