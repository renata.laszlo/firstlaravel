<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Task_Priority;
use App\Models\Task;

class TaskPriorityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_tasks()
    {
        $priority = factory(Task_Priority::class)->create();

        $task = factory(Task::class)->create(['priority_id' => $priority->id]);

        $this->assertTrue($priority->tasks->contains($task));
    }
}
