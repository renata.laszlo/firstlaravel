<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Task;
use App\Models\Project;
use App\Models\Task_Priority;
use App\Models\Task_Status;
use App\Models\User;

class TaskTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_project()
    {
        $task = factory(Task::class)->create();

        $this->assertInstanceOf(Project::class, $task->project);
    }

    /** @test */
    public function it_has_path()
    {
        $task = factory(Task::class)->create();

        $this->assertEquals('/projects/'.$task->project->id.'/tasks/'.$task->id, $task->path());
    }

    /** @test */
    public function it_belongs_to_a_priority()
    {
        $task = factory(Task::class)->create();

        $this->assertInstanceOf(Task_Priority::class, $task->priority);
    }

    /** @test */
    public function it_belongs_to_a_status()
    {
        $task = factory(Task::class)->create();

        $this->assertInstanceOf(Task_Status::class, $task->status);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $task = factory(Task::class)->create();

        $this->assertInstanceOf(User::class, $task->assignedTo);
    }
}
