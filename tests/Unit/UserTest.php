<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\Project;
use Illuminate\Database\Eloquent\Collection;

class UserTest extends TestCase
{
    /** @test */
    public function a_user_has_projects()
    {
        $user = factory(User::class)->create();

        // $this->assertInstanceOf(Collection::class, $user->projects);

        $project = factory(Project::class)->create(['owner_id' => $user->id]);

        $this->assertTrue($user->projects->contains($project));
    }

    /** @test */
    // public function a_user_can_have_assigned_projects()
    // {
    //     $user = factory(User::class)->create();


    // }

}
