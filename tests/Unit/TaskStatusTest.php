<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Task_Status;
use App\Models\Task;

class TaskStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_tasks()
    {
        $status = factory(Task_Status::class)->create();

        $task = factory(Task::class)->create(['status_id' => $status->id]);

        $this->assertTrue($status->tasks->contains($task));
    }
}
