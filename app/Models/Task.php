<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'project_id', 'assigned_to', 'priority_id', 'status_id', 'body', 'completed_at', 'deadline', 'notes', 'started_at', 'estimated_hours', 'total_hours'
    ];

    protected $touches = ['project'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function path()
    {
        return "/projects/{$this->project->id}/tasks/{$this->id}";
    }

    public function priority()
    {
        return $this->belongsTo(Task_Priority::class);
    }

    public function status()
    {
        return $this->belongsTo(Task_Status::class);
    }

    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function scopeGroupByStatus($query)
    {
        return $query->get()->groupBy('status_id');
    }

    public function scopeByProject($query, Project $project)
    {
        return $query->where('project_id', $project->id);
    }

}
