<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task_Priority extends Model
{
    use SoftDeletes;

    public $table = 'task_priority';

    protected $fillable = [
        'title', 'name', 'color'
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class, 'priority_id');

        // return $this->belongsToMany(Task::class, 'priority_id');
    }
}
