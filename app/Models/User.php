<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function projects()
    {
        return $this->hasMany(Project::class, 'owner_id')->latest('updated_at');
    }

    public function assignedProjects()
    {
        return $this->hasManyThrough(Project::class, Task::class, 'assigned_to', 'id', 'id', 'project_id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'assigned_to')->oldest('deadline');
    }

    public function allTasks()
    {
        $allOwnTasks = $this->hasManyThrough(Task::class, Project::class, 'owner_id', 'project_id')->get();
        return $allOwnTasks->merge($this->tasks);
    }

    public function sent()
    {
        return $this->hasMany(Message::class, 'sender_id')->oldest();
    }

    public function received()
    {
        return $this->hasMany(Message::class, 'receiver_id')->oldest();
    }

    public function sendMessageTo($attributes)
    {
        return $this->sent()->create($attributes);
    }

    public function unreadMessages()
    {
        return $this->received()->whereNull('read_at');
    }

    public function scopeExceptMe($query)
    {
        // return $query->except();
        return $query->where('id', '!=', auth()->id());
    }

    public function allProjects()
    {
        $own_projects = $this->projects;
        $assigned_projects = $this->assignedProjects;

        return $own_projects->merge($assigned_projects);
    }
}
