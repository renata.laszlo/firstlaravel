<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'description', 'owner_id', 'notes', 'completed_at',
    ];

    public function path()
    {
        return "/projects/{$this->id}";
    }

    public function tasks()
    {
        return $this->hasMany(Task::class)->oldest('deadline');
    }

    public function addTask($body)
    {
        return $this->tasks()->create($body);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function getCompletedTasks()
    {
        return $this->tasks()->whereNotNull('completed_at')->get();
    }

    public function scopeByDateRange($query, $column, $startdate, $enddate)
    {
        return $query->whereBetween($column, [$startdate, $enddate]);
    }

    public function scopeDateBetween($query, $start, $end, $date)
    {
        return $query->where([
                [$start, '<=', $date],
                [$end, '>=', $date]
            ]);
    }
}
