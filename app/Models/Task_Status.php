<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task_Status extends Model
{
    use SoftDeletes;

    public $table = 'task_status';

    protected $fillable = [
        'title', 'name', 'color'
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class, 'status_id')->oldest('deadline');
    }
 }
