<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Task;
use Illuminate\Support\Facades\Notification;
use App\Models\Task_Status;
use App\Notifications\ExpiredTask;

class Expired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily notification to users which task expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expired_status = Task_Status::where('name', 'expired')->first();
        if ( ! $expired_status ) {
            return $this->error('Status does not exist');
        }

        $tasks = Task::whereDate('deadline', '<', now())
            ->whereNull('completed_at')
            ->where('status_id', '!=', $expired_status->id);
            // ->get();

        if ( ! count($tasks->get()) ) {
            return $this->line('No tasks' . ' - ' . now());
        }

        foreach ($tasks->get() as $task) {
            Notification::send([$task->assignedTo, $task->project->owner], new ExpiredTask($task) );
        }

        $tasks->update(['status_id' => $expired_status->id]);

        $this->line('Sent a notification to users which task expired');
    }
}
