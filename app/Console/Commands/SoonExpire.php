<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Task;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SoonExpiresTask;

class SoonExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:soonExpires';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily notification to users which task expires soon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::whereBetween('deadline', [now(), now()->addDays(3) ])->get();

        foreach ($tasks as $task) {
            // Prevent duplicate notifications
            Notification::send([$task->assignedTo, $task->project->owner], new SoonExpiresTask($task) );
        }

        $this->line('Sent a notification to users which task expires soon');
    }
}
