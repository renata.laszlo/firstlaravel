<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectRepository implements ProjectRepositoryInterface
{
    protected $model;

    public function __construct(Project $model) {
        $this->model = $model;
    }

    /**
     * Get all projects
     *
     * @return void
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Get projects by user
     *
     * @param User $user
     * @return void
     */
    public function getByUser(User $user)
    {
        return $this->model->where('owner_id', $user->id)->get();
    }

    /**
     * Get assigned projects by user
     *
     * @param User $user
     * @return void
     */
    public function getAssignedProjectsByUser(User $user)
    {
        return $user->assignedProjects;
    }

    /**
     * Get owned projects and assigned projects by user
     *
     * @param User $user
     * @return void
     */
    public function getAllProjectsByUser(User $user)
    {
        $own_projects = $this->getByUser($user);
        $assigned_projects = $this->getAssignedProjectsByUser($user);

        return $own_projects->merge($assigned_projects);
    }

    /**
     * Update a project
     *
     * @param int $id
     * @param array $post_data
     * @return void
     */
    public function update($id, $post_data)
    {
        return $this->model->find($id)->update($post_data);
    }

    /**
     * Create a project to the logged user
     *
     * @param array $post_data
     * @return void
     */
    public function create($post_data)
    {
        return auth()->user()->projects()->create($post_data);
        // return $this->model->create($post_data);
    }

    /**
     * Show a project
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        $project = $this->model->findOrFail($id);
        $isOwner = false;
        if ( $this->isOwner($project) ){
            $isOwner = true;
            $tasks = $project->tasks()->groupByStatus();
        } else {
            $tasks = auth()->user()->tasks()->byProject($project)->groupByStatus();
        }
        $attributes['isOwner'] = $isOwner;
        $attributes['tasks'] = $tasks;
        $attributes['project'] = $project;
        return $attributes;
    }

    /**
     * Check the logged user is the project's owner
     *
     * @param Project $project
     * @return boolean
     */
    public function isOwner(Project $project)
    {
        return auth()->user()->is($project->owner);
    }

    /**
     * Delete a project
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
