<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use App\Models\Project;

interface ProjectRepositoryInterface
{
    public function all();

    public function getByUser(User $user);

    public function getAssignedProjectsByUser(User $user);

    public function getAllProjectsByUser(User $user);

    public function update($id, $post_data);

    public function create($post_data);

    public function show($id);

    public function isOwner(Project $project);

    public function delete($id);
}
