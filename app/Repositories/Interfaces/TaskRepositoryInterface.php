<?php

namespace App\Repositories\Interfaces;

use App\Models\Project;
use App\Models\Task;

interface TaskRepositoryInterface
{
    public function addTaskToProject(Project $project);

    public function isOwner(Project $project);

    public function updateTask($id);

    public function deleteTask($id);

    public function validateRequest();

    public function getUsersPrioritiesStatuses(Project $project = null, Task $task = null, $isOwner = false);

    public function statusHandling(Task $task, $new_status_id);

    public function updateStatus($id);

}
