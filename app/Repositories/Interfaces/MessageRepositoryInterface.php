<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

interface MessageRepositoryInterface
{
    public function getFirstUser();

    public function getMessagesByUser(User $user);

    public function getUsersExceptMe();

    public function sendMessage();

    public function markAllAsRead();

    public function markAsRead($id);

}
