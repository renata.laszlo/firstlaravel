<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\MessageRepositoryInterface;

class MessageRepository implements MessageRepositoryInterface
{

    protected $model;

    /**
     * Constructor
     *
     * @param Message $model
     */
    public function __construct(Message $model) {
        $this->model = $model;
    }


    /**
     * Get the first user except the logged user
     *
     * @return void
     */
    public function getFirstUser()
    {
        $users = User::exceptMe()->get();
        if ($users->count() == 0){
            return redirect()->route('message.create');
        }
        return $users->first();
    }

    /**
     * Get all messages by the user
     *
     * @param User $user
     * @return void
     */
    public function getMessagesByUser(User $user)
    {
        $attributes['users'] = User::exceptMe()->get();
        $attributes['messages'] = $this->model->byUser($user)->get();
        $attributes['selected_user'] = $user;
        return $attributes;
    }

    /**
     * Get users except the logged user
     *
     * @return void
     */
    public function getUsersExceptMe()
    {
        return User::exceptMe()->pluck('name', 'id');
    }

    /**
     * Send a message
     *
     * @return void
     */
    public function sendMessage()
    {
        $attributes = request()->validate([
            'body' => 'required|min:3',
            'receiver_id' => 'required'
        ]);

        auth()->user()->sendMessageTo($attributes);

        $user = User::findOrFail($attributes['receiver_id']);
        $messages = $this->model->byUser($user)->get();
        return $messages;
    }

    /**
     * Mark all as read
     *
     * @return void
     */
    public function markAllAsRead()
    {
        return auth()->user()->unreadMessages()->update(['read_at'=> now()]);
    }

    /**
     * Mark as read
     *
     * @param int $id
     * @return void
     */
    public function markAsRead($id)
    {
        return $this->model->findOrFail($id)->update(['read_at' => now()]);
    }
}
