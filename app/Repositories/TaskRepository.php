<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TaskRepositoryInterface;
use App\Models\Task;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AssignedTask;
use App\Models\Project;
use App\Models\User;
use App\Models\Task_Priority;
use App\Models\Task_Status;

class TaskRepository implements TaskRepositoryInterface
{
    protected $model;

    public function __construct(Task $model) {
        $this->model = $model;
    }

    /**
     * Add a new task to a project
     *
     * @param Project $project
     * @return void
     */
    public function addTaskToProject(Project $project)
    {
        $task = $project->addTask($this->validateRequest());

        Notification::send($task->assignedTo, new AssignedTask($task) );

        return $task;
    }

    /**
     * The owner updates the task
     *
     * @param int $id
     * @return void
     */
    public function updateTask($id)
    {
        $task = $this->model->findOrFail($id);
        if ($this->isOwner($task->project)) {
            $attributes = $this->validateRequest();
        } else {
            $attributes = request()->validate([
                'status_id' => 'required',
                'notes' => '',
                'estimated_hours' => 'required',
                'total_hours' => 'required',
            ]);
        }
        $attributes = collect($attributes)->merge($this->statusHandling($task, $attributes['status_id']))->toArray();

        return $task->update($attributes);
    }

    /**
     * Check the logged user is the project's owner
     *
     * @param Project $project
     * @return boolean
     */
    public function isOwner(Project $project)
    {
        return auth()->user()->is($project->owner);
    }

    /**
     * Delete a task
     *
     * @param int $id
     * @return void
     */
    public function deleteTask($id)
    {
        return $this->model->delete($id);
    }

    /**
     * Validate the request
     *
     * @return void
     */
    public function validateRequest()
    {
        return request()->validate([
            'body' => 'required|min:3',
            'assigned_to' => 'required',
            'priority_id' => 'required',
            'status_id' => 'required',
            'completed_at' => '',
            'deadline' => 'required',
            'notes' => '',
            'estimated_hours' => 'required',
            'total_hours' => 'required',
        ]);
    }

    /**
     * Return all users, priorities, statuses
     * if need it, also return project, task, isOwner
     *
     * @param Project $project
     * @param Task $task
     * @param boolean $isOwner
     * @return array
     */
    public function getUsersPrioritiesStatuses(Project $project = null, Task $task = null, $isOwner = false)
    {
        if ($task){
            $attributes['task'] = $task;
        }
        if ($project){
            $attributes['project'] = $project;
        }
        if ($isOwner){
            $attributes['isOwner'] = $this->isOwner($project);
        }
        $attributes['users'] = User::all();
        $attributes['priorities'] = Task_Priority::all();
        $attributes['statuses'] = Task_Status::all();
        return $attributes;
    }

    /**
     * Handle the status
     *
     * @param Task $task
     * @param int $new_status_id
     * @return array
     */
    public function statusHandling(Task $task, $new_status_id)
    {
        $new_status = Task_Status::findOrFail($new_status_id);

        // expired task
        if ($task->status->name == 'expired') {
            $attributes['status_id'] = $task->status->id;
            $attributes['completed_at'] = ($new_status->name == 'done') ? (now()) : (null);
            return $attributes;
        }

        $attributes['completed_at'] = null;

        switch ($new_status->name) {
            case 'not_started':
                $attributes['started_at'] = null;
                break;
            case 'in_progress':
                $attributes['started_at'] = now();
                break;
            case 'done':
                $attributes['completed_at'] = now();
                break;
            case 'expired':
                return [];
            default:
                break;
        }
        return $attributes;
    }

    /**
     * Update the status
     *
     * @param int $id
     * @return void
     */
    public function updateStatus($id)
    {
        $task = $this->model->findOrFail($id);

        $statuses = Task_Status::all();

        $attributes['completed_at'] = null;

        if (request()->has('completed')) {
            switch ($task->status->name) {
                case 'not_started':
                    $attributes['status_id'] = $statuses->firstWhere('name', 'in_progress')->id;
                    $attributes['started_at'] = now();
                    break;
                case 'in_progress':
                    $attributes['status_id'] = $statuses->firstWhere('name', 'done')->id;
                    $attributes['completed_at'] = now();
                    break;
                case 'done':
                    $attributes['completed_at'] = now();
                    break;
                case 'expired':
                    $attributes['completed_at'] = now();
                default:
                    break;
            }
        } elseif ($task->status->name == 'done') {
            $attributes['status_id'] = $statuses->firstWhere('name', 'in_progress')->id;
        }

        return $task->update($attributes);
    }
}

