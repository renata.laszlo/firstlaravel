<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Task;
use App\Models\Project;

class ProjectTasksPolicy
{
    use HandlesAuthorization;


    public function view(User $user, Task $task)
    {
        return $user->is($task->project->owner) || $user->is($task->assignedTo);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, Project $project)
    {
        return $user->is($project->owner);
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        return $user->is($task->project->owner) || $user->is($task->assignedTo);
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        return $user->is($task->project->owner);
    }

}
