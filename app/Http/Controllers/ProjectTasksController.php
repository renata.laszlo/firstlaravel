<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Task;
use App\Repositories\Interfaces\TaskRepositoryInterface;

class ProjectTasksController extends Controller
{

    protected $task;

    public function __construct(TaskRepositoryInterface $task) {
        $this->task = $task;
    }

    /**
     * Show the create task form
     *
     * @param Project $project
     * @return void
     */
    public function create(Project $project)
    {
        return view('tasks.create', $this->task->getUsersPrioritiesStatuses($project));
    }

    /**
     * Add a new task to a project
     *
     * @param Project $project
     * @return void
     */
    public function store(Project $project)
    {
        $this->task->addTaskToProject($project);

        return redirect($project->path());
    }

    /**
     * Show the edit task form
     *
     * @param Project $project
     * @param Task $task
     * @return void
     */
    public function edit(Project $project, Task $task)
    {
        return view('tasks.edit', $this->task->getUsersPrioritiesStatuses($project, $task, true));
    }

    /**
     * Update a task
     *
     * @param Project $project
     * @param Task $task
     * @return void
     */
    public function update(Project $project, Task $task)
    {
        $this->task->updateTask($task->id);

        return redirect($project->path());
    }

    /**
     * Delete a task
     *
     * @param Task $task
     * @return void
     */
    public function destroy(Task $task)
    {
        $this->task->deleteTask($task->id);

        return url()->previous();
    }

    /**
     * View a task
     *
     * @param Project $project
     * @param Task $task
     * @return void
     */
    public function show(Project $project, Task $task)
    {
        return view('tasks.show', compact('task', 'project'));
    }

    /**
     * Assigned user updates the task status
     *
     * @param Project $project
     * @param Task $task
     * @return void
     */
    public function statusChanges(Project $project, Task $task)
    {
        $this->task->updateStatus($task->id);

        return redirect($project->path());
    }

}
