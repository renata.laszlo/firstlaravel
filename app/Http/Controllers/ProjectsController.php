<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Repositories\Interfaces\ProjectRepositoryInterface;

class ProjectsController extends Controller
{
    protected $project;

    /**
     * Constructor
     *
     * @param ProjectRepositoryInterface $project
     */
    public function __construct(ProjectRepositoryInterface $project) {
        $this->project = $project;
    }

    /**
     * Display a listing of the projects
     *
     * @return void
     */
    public function index()
    {
        $projects = $this->project->getAllProjectsByUser(auth()->user());

        return view('projects.index', compact('projects'));
    }

    /**
     * Show the create form
     *
     * @return void
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Create a new project
     *
     * @return void
     */
    public function store()
    {
        $project = $this->project->create($this->validateRequest());

        return redirect($project->path());
    }

    /**
     * Show a project
     *
     * @param Project $project
     * @return void
     */
    public function show(Project $project)
    {
        $attributes = $this->project->show($project->id);

        return view('projects.show', $attributes);
    }

    /**
     * Edit a project
     *
     * @param Project $project
     * @return void
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update a project
     *
     * @param Project $project
     * @return void
     */
    public function update(Project $project)
    {
        $this->project->update($project->id, request()->validate(['notes' => 'min:3']));

        return redirect($project->path());
    }

    /**
     * Validate the request
     *
     * @return void
     */
    protected function validateRequest()
    {
        return request()->validate([
            'title' => 'required',
            'description' => 'required',
            'notes' => 'min:3'
        ]);
    }

    /**
     * Delete a project
     *
     * @param Project $project
     * @return void
     */
    public function destroy(Project $project)
    {
        $this->project->delete($project->id);

        return redirect('/projects');
    }

}
