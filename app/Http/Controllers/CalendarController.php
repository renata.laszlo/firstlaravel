<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Calendar;
use App\Models\Project;
use App\Models\Task_Status;

class CalendarController extends Controller
{
    /**
     * Show all tasks of the selected project of the logged user
     *
     * @return void
     */
    public function index()
    {
        $projects = auth()->user()->allProjects();
        $selected_project = 0;

        $statuses = Task_Status::all();

        return view('calendar.index', compact('projects', 'selected_project', 'statuses'));
    }

    /**
     * Return an array of tasks of the selected project
     *
     * @return void
     */
    public function getTasks()
    {
        $selected_project = request('project');

        if ($selected_project == 0){
            $tasks = auth()->user()->allTasks();
        } else {
            $project = Project::findOrFail($selected_project);
            if (auth()->user()->is($project->owner)) {
                $tasks = $project->tasks;
            } else {
                $tasks = auth()->user()->tasks()->byProject($project)->get();
            }
        }

        foreach ($tasks as &$task) {
            $task->path = $task->path();
            // $task->priority = $task->priority;
            $task->status = $task->status;
            $task->assigned_to = $task->assignedTo;
        }

        return $tasks;
    }
}
