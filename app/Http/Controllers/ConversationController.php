<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\Interfaces\MessageRepositoryInterface;

class ConversationController extends Controller
{
    protected $message;

    /**
     * Constructor
     *
     * @param MessageRepositoryInterface $message
     */
    public function __construct(MessageRepositoryInterface $message) {
        $this->message = $message;
    }

    /**
     * Show messages by the first user
     *
     * @return void
     */
    public function index()
    {
        return redirect()->route('message.show', ['user' => $this->message->getFirstUser()]);
    }

    /**
     * Show messages by the user
     *
     * @param User $user
     * @return void
     */
    public function show(User $user)
    {
        return view('messages.index', $this->message->getMessagesByUser($user));
    }

    /**
     * Create a new message
     *
     * @return void
     */
    public function create()
    {
        return view('messages.create', ['users' => $this->message->getUsersExceptMe()]);
    }

    /**
     * Send a message
     *
     * @return void
     */
    public function sendMessage()
    {
        return view('messages.includes.items', ['messages' => $this->message->sendMessage()]);
    }

    /**
     * Mark all messages as read
     *
     * @return void
     */
    public function markAllAsRead()
    {
        $this->message->markAllAsRead();
        return redirect()->route('message.index');
    }

    /**
     * Mark a message as read
     *
     * @param Message $message
     * @return void
     */
    public function markAsRead(Message $message)
    {
        $this->message->markAsRead($message->id);

        return redirect()->route('message.show', ['user' => $message->sender]);
    }

}
