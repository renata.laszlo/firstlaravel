<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class Notifications extends Controller
{

    /**
     * Mark a notification as read
     *
     * @param int $notification_id
     * @return void
     */
    public function markAsRead($notification_id)
    {
        $notification = auth()->user()->notifications()->find($notification_id);

        if ( ! $notification ) {
            return back();
        }

        $notification->markAsRead();

        $task = Task::find($notification->data['task']['id']);
        if ( ! $task ){
            return redirect('/projects/'.$notification->data['task']['project_id']);
        }

        return redirect($task->path());
    }

    /**
     * Mark all notifications as read
     *
     * @return void
     */
    public function markAllAsRead()
    {
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }
}
