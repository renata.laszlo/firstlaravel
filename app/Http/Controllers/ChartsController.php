<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Task;

class ChartsController extends Controller
{
    /**
     * View the chart
     *
     * @return void
     */
    public function index()
    {
        return view('charts.index');
    }

    /**
     * Return an array of tasks group by project
     *
     * @return void
     */
    public function getTasks()
    {
        $dates = request();

        $tasks_deadline = Task::selectRaw('project_id, MAX(deadline) as deadline')
            ->groupBy('project_id');

        $projects = Project::joinSub($tasks_deadline, 'tasks', function ($join) {
                $join->on('projects.id', '=', 'tasks.project_id');
            })
            ->dateBetween( 'created_at', 'deadline', date('Y-m-d', strtotime($dates['start'])) )
            ->orWhere
            ->dateBetween( 'created_at', 'deadline', date('Y-m-d', strtotime($dates['end'])) )
            ->orWhere
            ->byDateRange('created_at', date('Y-m-d', strtotime($dates['start'])), date('Y-m-d', strtotime($dates['end'])) )
            ->orWhere
            ->byDateRange('deadline', date('Y-m-d', strtotime($dates['start'])), date('Y-m-d', strtotime($dates['end'])) )
            ->get();

        foreach ($projects as &$project) {
            $project->tasks = (auth()->user()->is($project->owner)) ? $project->tasks : auth()->user()->tasks()->byProject($project)->get();
            foreach ($project->tasks as &$task) {
                $task->completed =  ($task->estimated_hours) ? ($task->total_hours / $task->estimated_hours) : 0;
            }
        }

        return $projects;
    }
}
